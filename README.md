# README #

Finds new terms added to the end of Bikeup's en.txt localization file, translates them using Google Translate API and appends the translations to the rest of the localization files.

### Setup

1. Set up a Kotlin development environment https://kotlinlang.org/docs/tutorials/getting-started.html
2. Get a Google translate API key https://cloud.google.com/translate/docs/reference/libraries#client-libraries-install-java
3. The rest gets changed all the time - glhf
