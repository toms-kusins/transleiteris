package com.smot.localizer

import com.google.cloud.translate.Translate
import com.google.cloud.translate.TranslateOptions
import com.google.cloud.translate.Translation
import java.io.File

/**
 * Setup:
 * 1. https://cloud.google.com/translate/docs/referenceTerms/libraries#client-libraries-install-java
 * 2. Update `LANG_PATH` in Main.kt
 * 3. Call translate with `termsToTranslate` set to the number of new terms or -1 to auto-detect
 */

/** Public interface */

fun translateLastLines(count: Int = -1, mock: Boolean = false) = translate(mock) {
    getNLastTermsToTranslate(count)
}

fun translateAllMissing(mock: Boolean = false) = translate(mock) {
    getAllUntranslatedTerms()
}

fun replaceWordInTerm(key: String, word: String, replacement: String, excludeLangs: List<String>)
    = replaceWord(key, word, replacement, excludeLangs)

private val langCodeMap = mapOf(
    "dk" to "da",
    "tk" to "tr",
    "vn" to "vi"
)

val File.lang get() = langCodeMap.getOrDefault(nameWithoutExtension, nameWithoutExtension)

// translator gets string with |param|s replaced with these numbers
val parameterConstants = mapOf(
    "|param|" to "13372",
    "|param1|" to "13373",
    "|param2|" to "13374",
    "|param3|" to "13375"
)

private val service = TranslateOptions.getDefaultInstance().service

private fun translate(mock: Boolean = false, getTerms: List<Pair<String, String>>.() -> List<Pair<String, String>>) {
    otherLangFiles.forEach { file ->
        file.splitIntoTerms()
            .run { this to getTerms() }
            .also {
                if (mock) mockTranslateLang(file, it.second)
                else (it.first + translateLang(file, it.second)).createLangFile(file)
            }

    }
}

private fun List<Pair<String, String>>.getNLastTermsToTranslate(count: Int = -1): List<Pair<String, String>> {
    val reversed = defaultLangFile.splitIntoTerms().reversed()
    val index = count
        .takeUnless { it == -1 }
        ?: reversed.indexOfFirst { r -> any { it.first == r.first } }
//    return listOf("\"UI:PostGame:Rewardsx2\"" to "\"Rewards x2\"")
    return reversed.subList(0, index).reversed()
}

private fun List<Pair<String, String>>.getAllUntranslatedTerms(): List<Pair<String, String>> {
    return defaultLangFile.splitIntoTerms().filter { reference -> none { it.first == reference.first } }
}

private fun mockTranslateLang(file: File, terms: List<Pair<String, String>>) = terms
    .also { println("Translating ${file.nameWithoutExtension}") }
    .map { println("\t$it") }
    .let { emptyList<Pair<String, String>>() }

private class TranslatedTerm(
    val key: String,
    val original: String,
    val translated: String
)

private fun translateLang(file: File, terms: List<Pair<String, String>>): List<Pair<String, String>> = file
    .lang
    .also { println("Translating " + it) }
    .let { lang ->  terms
        .map {
            TranslatedTerm(
                it.first,
                it.second,
                translateTerm(it.second, lang).translatedText.reParametrize()
            )
        }
        .map {
            val capitalize = it.original.firstOrNull()?.isUpperCase() == true
            it.key to
                if (capitalize) it.translated.capitalize().reParametrize()
                else it.translated.reParametrize()
        }
        .onEach { println("\tresult: ${it.first to it.second}") }
    }

fun translateTerm(it: String, targetLang: String, sourceLang: String = "en"): Translation {
    return service.translate(
        it.trim('"', ';').deParametrize(),
        Translate.TranslateOption.sourceLanguage(sourceLang),
        Translate.TranslateOption.targetLanguage(targetLang)
    )
}

private fun String.deParametrize() = parameterConstants
    .entries
    .fold(this) { acc, e -> acc.replace(e.key, e.value) }

private fun String.reParametrize() = parameterConstants
    .entries
    .fold(this) { acc, e -> acc.replace(e.value, e.key) }

private fun replaceWord(key: String, word: String, replacement: String, excludeLangs: List<String>) = otherLangFiles
    .filter { it.lang !in excludeLangs }
    .forEach { file ->
        print("(${file.lang}) ")
        file.splitIntoTerms()
            .firstOrNull { it.first.trim('"') == key }
            ?.also { og ->
                og.second
                    .split(' ')
                    .let { it + it.windowed(2, 1) { "${it[0]} ${it[1]}" } }
                    .firstOrNull {
                        val trimmed = it.trim('.', '!', '?', ',', ';', '"')
                        word.toLowerCase() == translateTerm(trimmed, DEFAULT_LANG, file.lang)
                            .translatedText
                            .toLowerCase()
                    }
                    ?.let { it to translateTerm(replacement, file.lang, DEFAULT_LANG).translatedText }
                    ?.let {
                        val newValue = og.second.replace(it.first, it.second, ignoreCase = true)
                        println(newValue)
                        val terms = file.splitIntoTerms().toMutableList()
                        val index = terms.indexOfFirst { it.first.trim('"') == key }
                        terms[index] = "\"$key\"" to newValue
                        terms
                    }
                    ?.createLangFile(file)
                    ?: "<none>".also { println("not found (${og.second})") }
            }
            ?: Unit.also { println("Key not found in ${file.lang}") }
    }
