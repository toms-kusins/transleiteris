package com.smot.localizer

import java.io.File

/**
 * Input selection
 */

val langFiles = File("C:\\code\\bikeup22\\Assets\\Resources\\Data\\")
    .listFiles()
    .filter { it.extension == FILE_EXTENSION }

val defaultLangFile: File = langFiles.first { it.lang == DEFAULT_LANG }
val otherLangFiles = langFiles.filter { it.lang != DEFAULT_LANG }

/**
 * Parsing
 */

@Deprecated(
    "Returns keys with quotation marks and values with quot.marks and semicolons which is dumb",
    ReplaceWith("File.terms()")
)
fun File.splitIntoTerms(): List<Pair<String, String>> = readText()
    .split('\n')
    .map { it.split(TERM_VALUE_SEPARATOR) }
    .filter { it.size == 2 }
    .map { it[0] to it[1] }

private fun String.trimKey() = trim('"', 65279.toChar())
private fun String.trimValue() = trim(';').trim('"', 65279.toChar())

fun File.terms() = readText()
    .split('\n')
    .map { it.split(TERM_VALUE_SEPARATOR) }
    .filter { it.size == 2 }
    .map { it[0].trimKey() to it[1].trimValue() }

/**
 * Output
 */

fun List<Pair<String, String>>.createLangFile(file: File) = this
    .map {
        it.first.trimKey() to it.second.trimValue()
    }
    .joinToString(
        separator = ";\n",
        postfix = ";\n"
    ) {
        "\"${it.first}\"$TERM_VALUE_SEPARATOR\"${it.second}\""
    }
    .also {
        file.writeText(it, Charsets.UTF_8)
    }
