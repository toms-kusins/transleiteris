package com.smot.localizer

import com.google.common.collect.Sets

fun printTranslations(term: String) = langFiles
    .map { file ->
        file.splitIntoTerms()
            .firstOrNull { it.first == "\"$term\"" }
            .also { println("${file.lang} ${it?.second}") }
    }

fun printReverseTranslations(term: String, excludeLangs: List<String> = emptyList()) = otherLangFiles
    .filter { it.lang !in excludeLangs}
    .map { file ->
        print("${file.lang}: ")
        file.splitIntoTerms()
            .firstOrNull { it.first == "\"$term\"" }
            ?.let { translateTerm(it.second, "en", file.lang) }
            .also { println(it?.translatedText ?: "<none!>") }
    }

fun printMatching(includeLangs: List<String>? = null, matcher: (Pair<String, String>) -> Boolean) {
    langFiles
        .filter { includeLangs == null || it.lang in includeLangs }
        .map { file ->
            println("${file.lang}: ")
            file.splitIntoTerms()
                .filter(matcher)
                .onEach(::println)
        }
}

fun printKeySetDifferencesInMatches(matcher: (Pair<String, String>) -> Boolean) {
    val reference = defaultLangFile
        .splitIntoTerms()
        .filter(matcher)
        .map { it.first }
        .toSet()

    val refterms = defaultLangFile.splitIntoTerms().toMap()
    otherLangFiles
        .filter { it.lang == "en2" }
        .map { file ->
            val terms = file.splitIntoTerms().toMap()

            file.splitIntoTerms()
                .filter(matcher)
                .map { it.first }
                .toSet()
                .let {
                    val common = Sets.intersection(reference, it)
                    (reference - common).map { "r: ${it}" } + (it - common).map { "t: ${it}" }
                }
                .joinToString("\n") { "\t$it" }
                .also { println("${file.lang}\n$it") }
        }

}