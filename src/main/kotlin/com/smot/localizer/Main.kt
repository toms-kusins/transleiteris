package com.smot.localizer

const val DEFAULT_LANG = "en"
const val TERM_VALUE_SEPARATOR = " = "
const val FILE_EXTENSION = "txt"

fun main(args: Array<String>) {
    /** Stats & Diagnostics */

//    printLineCounts()

//    printTranslations("FBInviteTitle")
//    printReverseTranslations("UI:PostGame:Rewardsx2")

//    printMatching(listOf("en", "de")) {
//        it.second.contains("Snow Trial")
//    }
//    printKeySetDifferencesInMatches {
////        it.second.contains("Elite Trials")
//        true
//    }

    /** Auto-translation */

//    translateLastLines(count = 1, mock = false)
//    translateAllMissing(mock = false)
//    replaceWordInTerm(
//        "FBInviteTitle",
//        "snow",
//        "bike",
//        excludeLangs = listOf("th") // th does not split well :(
//    )

    /** Clean-up */

//    removeRedundantTranslations()
//    normalizeSeparators()
//    orderAlphabetically()

}
