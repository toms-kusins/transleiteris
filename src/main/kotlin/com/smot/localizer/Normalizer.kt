package com.smot.localizer


fun normalizeSeparators() {
    langFiles.map { file ->
        file.readText()
            .split(";\\n|\\n".toRegex())
            .map { it.split("[\\s\\xa0]*=[\\s\\xa0]*".toRegex()) }
            .filter {
                if (it.size != 2) {
                    println("(${file.lang}) Skipping term: $it")
                }
                it.size == 2
            }
            .map { it[0] to it[1] }
            .createLangFile(file)
    }
}

fun orderAlphabetically() {
    langFiles.map {
        it.splitIntoTerms()
            .sortedBy { it.first.trim('"') }
            .createLangFile(it)
    }
}