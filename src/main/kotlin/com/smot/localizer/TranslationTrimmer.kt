package com.smot.localizer

fun removeRedundantTranslations() {
    val reference = defaultLangFile.terms()
    println("Total terms: ${reference.size}")
    langFiles.forEach { file ->
        var skippedTerms = 0
        val contents = file.terms()

        contents.filter { translated ->
            reference.any { it.first == translated.first }.also {
                if (!it) {
                    println("(${file.lang}) Skipping: ${translated.first}")
                    skippedTerms++
                }
            }
        }.createLangFile(file)

        println("${file.nameWithoutExtension} skipped $skippedTerms terms")
    }
}

fun printLineCounts() {
    langFiles.forEach { file ->
        file.readLines().also { println("${file.nameWithoutExtension}: ${it.size} lines") }
    }
}
